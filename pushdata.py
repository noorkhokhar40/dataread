import json
import urllib3
import datetime
import os, time
import schedule
import sys



URL = "http://apps.dignous.com/dignousraw/smarttemperature/add"
METHOD = "POST"
CONTENTTYPE = "application/json";

#Time
EVERY_SECONDS1 = 60  # SECONDS * WITH  MINTS
EVERY_SECONDS11 = 120
EVERY_SECONDS12 = 180
EVERY_SECONDS13 = 240
EVERY_SECONDS14 = 300


EVERY_SECONDS2 = 60
EVERY_SECONDS21 = 120
EVERY_SECONDS22 = 180
EVERY_SECONDS23 = 240
EVERY_SECONDS24 = 300


EVERY_SECONDS3 = 60
EVERY_SECONDS31 = 120
EVERY_SECONDS32 = 180
EVERY_SECONDS33 = 240
EVERY_SECONDS34 = 300


EVERY_SECONDS4 = 60
EVERY_SECONDS41 = 120
EVERY_SECONDS42 = 180
EVERY_SECONDS43 = 240
EVERY_SECONDS44 = 300



EVERY_SECONDS5 = 60
EVERY_SECONDS51 = 120
EVERY_SECONDS52 = 180
EVERY_SECONDS53 = 240
EVERY_SECONDS54 = 300



EVERY_SECONDS6 = 60
EVERY_SECONDS61 = 120
EVERY_SECONDS62 = 180
EVERY_SECONDS63 = 180
EVERY_SECONDS64 = 180


EVERY_SECONDS7 = 60
EVERY_SECONDS71 = 120
EVERY_SECONDS72 = 180
EVERY_SECONDS73 = 240
EVERY_SECONDS74 = 300


EVERY_SECONDS8 = 60
EVERY_SECONDS81 = 120
EVERY_SECONDS82 = 180
EVERY_SECONDS83 = 240
EVERY_SECONDS84 = 300


EVERY_SECONDS9 = 60
EVERY_SECONDS91 = 120
EVERY_SECONDS92 = 180
EVERY_SECONDS93 = 240
EVERY_SECONDS94 = 300



EVERY_SECONDS10 = 60
EVERY_SECONDS101 = 120
EVERY_SECONDS102 = 180
EVERY_SECONDS103 = 240
EVERY_SECONDS104 = 300

EVERY_SECONDS11 = 60
EVERY_SECONDS111 = 120
EVERY_SECONDS112 = 180
EVERY_SECONDS113 = 240
EVERY_SECONDS114 = 300

EVERY_SECONDS12 = 60
EVERY_SECONDS121 = 180
EVERY_SECONDS122 = 240
EVERY_SECONDS123 = 180
EVERY_SECONDS124 = 300

EVERY_SECONDS13 = 60
EVERY_SECONDS131 = 120
EVERY_SECONDS132 = 180
EVERY_SECONDS133 = 240
EVERY_SECONDS134 = 300

EVERY_SECONDS14 = 60
EVERY_SECONDS141 = 120
EVERY_SECONDS142 = 180
EVERY_SECONDS143 = 240
EVERY_SECONDS144 = 300

EVERY_SECONDS15 = 60
EVERY_SECONDS151 = 120
EVERY_SECONDS152 = 180
EVERY_SECONDS153 = 240
EVERY_SECONDS154 = 300

EVERY_SECONDS16 = 60
EVERY_SECONDS161 = 120
EVERY_SECONDS162 = 180
EVERY_SECONDS163 = 240
EVERY_SECONDS164 = 300

EVERY_SECONDS17 = 60
EVERY_SECONDS171 = 120
EVERY_SECONDS172 = 180
EVERY_SECONDS173 = 240
EVERY_SECONDS174 = 300

EVERY_SECONDS18 = 60
EVERY_SECONDS181 = 120
EVERY_SECONDS182 = 180
EVERY_SECONDS183 = 240
EVERY_SECONDS184 = 300

EVERY_SECONDS19 = 60
EVERY_SECONDS191 = 120
EVERY_SECONDS192 = 180
EVERY_SECONDS193 = 240
EVERY_SECONDS194 = 300

EVERY_SECONDS20 = 60
EVERY_SECONDS201 = 120
EVERY_SECONDS202 = 180
EVERY_SECONDS203 = 240
EVERY_SECONDS204 = 300

EVERY_SECONDS21 = 60
EVERY_SECONDS211 = 120 
EVERY_SECONDS212 = 180 
EVERY_SECONDS213 = 240 
EVERY_SECONDS214 = 300 

EVERY_SECONDS22 = 60
EVERY_SECONDS221 = 120 
EVERY_SECONDS222 = 180 
EVERY_SECONDS223 = 240 
EVERY_SECONDS224 = 300 

EVERY_SECONDS23 = 60
EVERY_SECONDS231 = 120
EVERY_SECONDS232 = 180
EVERY_SECONDS233 = 240
EVERY_SECONDS234 = 300


EVERY_SECONDS24 = 60
EVERY_SECONDS241 = 120 
EVERY_SECONDS242 = 180 
EVERY_SECONDS243 = 240 
EVERY_SECONDS244 = 300 


#deviceId=1&temperature=3.6&ph=8.2&tds=6.2&tts=2.2&conductivity=1.2&chlorine=5.2&nh3=9.2&nox=8.2&sox=1.2&co2=5.2&tss=2.2&cl=3.8&mg=3.2                                                                                             
##
def sendData1():	
    data = {"houseId":1,"shadeId":11,"deviceId": 101,"ph":6.7,"tds":305,"turbidity": 46,"watemp":19.5}
    pushToCloud(data); 

def sendData11():	
    data = {"houseId":1,"shadeId":11,"deviceId": 101,"ph":6.9,"tds":302,"turbidity": 54,"watemp":18.8}
    pushToCloud(data); 

def sendData12():	
    data = {"houseId":1,"shadeId":11,"deviceId": 101,"ph":6.5,"tds":299,"turbidity": 48,"watemp":19.5}
    pushToCloud(data); 


def sendData13():	
    data = {"houseId":1,"shadeId":11,"deviceId": 101,"ph":7.0,"tds":300,"turbidity": 51,"watemp":16.9}
    pushToCloud(data); 


def sendData14():	
    data = {"houseId":1,"shadeId":11,"deviceId": 101,"ph":6.8,"tds":294,"turbidity": 47,"watemp":18.9}
    pushToCloud(data); 

def sendData2():	
    data = {"houseId":1,"shadeId":11,"deviceId": 102,"ammonia":5,"carbonMonoxide":32,"carbonDioxide": 420,"oxygen":17.5, "temprature":28.9,"humidity":53.9}
    pushToCloud(data); 


def sendData21():	
    data = {"houseId":1,"shadeId":11,"deviceId": 102,"ammonia":4.2,"carbonMonoxide":29,"carbonDioxide": 390,"oxygen":17.9, "temprature":29.5,"humidity":58.9}
    pushToCloud(data); 


def sendData22():	
    data = {"houseId":1,"shadeId":11,"deviceId": 102,"ammonia":4.3,"carbonMonoxide":37,"carbonDioxide": 400,"oxygen":18.9, "temprature":27.9,"humidity":55.9}
    pushToCloud(data); 

def sendData23():	
    data = {"houseId":1,"shadeId":11,"deviceId": 102,"ammonia":5.2,"carbonMonoxide":32,"carbonDioxide": 370,"oxygen":17.5, "temprature":22.6,"humidity":67.9}
    pushToCloud(data); 

def sendData24():	
    data = {"houseId":1,"shadeId":11,"deviceId": 102,"ammonia":4.5,"carbonMonoxide":40,"carbonDioxide": 399,"oxygen":18.5, "temprature":21.9,"humidity":53.9}
    pushToCloud(data); 

def sendData3():	
    data = {"houseId":1,"shadeId":11,"deviceId": 103,"ph":6.5,"tds":300,"turbidity": 50,"watemp":20.2}
    pushToCloud(data); 

def sendData31():	
    data = {"houseId":1,"shadeId":11,"deviceId": 103,"ph":6.9,"tds":299,"turbidity": 44,"watemp":18.2}
    pushToCloud(data); 


def sendData32():	
    data = {"houseId":1,"shadeId":11,"deviceId": 103,"ph":6.2,"tds":294,"turbidity": 47,"watemp":17.5}
    pushToCloud(data); 


def sendData33():	
    data = {"houseId":1,"shadeId":11,"deviceId": 103,"ph":6.5,"tds":300,"turbidity": 50,"watemp":17.9}
    pushToCloud(data); 


def sendData34():	
    data = {"houseId":1,"shadeId":11,"deviceId": 103,"ph":6.8,"tds":287,"turbidity": 46,"watemp":18.9}
    pushToCloud(data); 


def sendData4():	
    data = {"houseId":1,"shadeId":11,"deviceId": 104,"ammonia":5,"carbonMonoxide":32,"carbonDioxide": 350,"oxygen":17.5, "temprature":28.9,"humidity":53.9}
    pushToCloud(data); 


def sendData41():	
    data = {"houseId":1,"shadeId":11,"deviceId": 104,"ammonia":4.5,"carbonMonoxide":30,"carbonDioxide": 370,"oxygen":18.9, "temprature":25.9,"humidity":57.4}
    pushToCloud(data); 

def sendData42():	
    data = {"houseId":1,"shadeId":11,"deviceId": 104,"ammonia":5.1,"carbonMonoxide":27,"carbonDioxide": 389,"oxygen":18.7, "temprature":30.9,"humidity":54.6}
    pushToCloud(data); 


def sendData43():	
    data = {"houseId":1,"shadeId":11,"deviceId": 104,"ammonia":4.2,"carbonMonoxide":36,"carbonDioxide": 384,"oxygen":16.5, "temprature":30.9,"humidity":55.7}
    pushToCloud(data); 

def sendData44():	
    data = {"houseId":1,"shadeId":11,"deviceId": 104,"ammonia":3.5,"carbonMonoxide":31,"carbonDioxide": 390,"oxygen":17.9, "temprature":30.9,"humidity":56.8}
    pushToCloud(data); 


def sendData5():	
    data = {"houseId":1,"shadeId":21,"deviceId": 201,"ph":6.9,"tds":385,"turbidity": 49,"watemp":19.1}
    pushToCloud(data); 

def sendData51():	
    data = {"houseId":1,"shadeId":21,"deviceId": 201,"ph":6.7,"tds":287,"turbidity": 47,"watemp":19.5}
    pushToCloud(data); 

def sendData52():	
    data = {"houseId":1,"shadeId":21,"deviceId": 201,"ph":6.4,"tds":298,"turbidity": 42,"watemp":18.5}
    pushToCloud(data); 

def sendData53():	
    data = {"houseId":1,"shadeId":21,"deviceId": 201,"ph":7.1,"tds":288,"turbidity": 50,"watemp":17.4}
    pushToCloud(data); 

def sendData54():	
    data = {"houseId":1,"shadeId":21,"deviceId": 201,"ph":6.7,"tds":305,"turbidity": 44,"watemp":18.3}
    pushToCloud(data); 

def sendData6():	
    data = {"houseId":1,"shadeId":21,"deviceId": 202,"ammonia":3.2,"carbonMonoxide":40,"carbonDioxide": 290,"oxygen":19.7, "temprature":27.5,"humidity":49.5}
    pushToCloud(data); 

def sendData61():	
    data = {"houseId":1,"shadeId":21,"deviceId": 202,"ammonia":5.2,"carbonMonoxide":38,"carbonDioxide": 399,"oxygen":17.8, "temprature":28.5,"humidity":44.5}
    pushToCloud(data); 

def sendData62():	
    data = {"houseId":1,"shadeId":21,"deviceId": 202,"ammonia":4.2,"carbonMonoxide":49,"carbonDioxide": 380,"oxygen":17.8, "temprature":22.5,"humidity":68.5}
    pushToCloud(data); 

def sendData63():	
    data = {"houseId":1,"shadeId":21,"deviceId": 202,"ammonia":4.9,"carbonMonoxide":47,"carbonDioxide": 400,"oxygen":18.2, "temprature":26.5,"humidity":54.5}
    pushToCloud(data); 

def sendData64():	
    data = {"houseId":1,"shadeId":21,"deviceId": 202,"ammonia":4.5,"carbonMonoxide":39,"carbonDioxide": 310,"oxygen":16.2, "temprature":25.5,"humidity":58.5}
    pushToCloud(data); 

def sendData7():	
    data = {"houseId":1,"shadeId":21,"deviceId": 203,"ph":6.8,"tds":305,"turbidity": 39,"watemp":20.5}
    pushToCloud(data); 

def sendData71():	
    data = {"houseId":1,"shadeId":21,"deviceId": 203,"ph":6.5,"tds":335,"turbidity": 48,"watemp":21.8}
    pushToCloud(data); 

def sendData72():	
    data = {"houseId":1,"shadeId":21,"deviceId": 203,"ph":7.1,"tds":299,"turbidity": 45,"watemp":20.4}
    pushToCloud(data); 

def sendData73():	
    data = {"houseId":1,"shadeId":21,"deviceId": 203,"ph":6.6,"tds":305,"turbidity": 49,"watemp":21.5}
    pushToCloud(data); 

def sendData74():	
    data = {"houseId":1,"shadeId":21,"deviceId": 203,"ph":7.1,"tds":287,"turbidity": 50,"watemp":19.5}
    pushToCloud(data); 


def sendData8():	
    data = {"houseId":1,"shadeId":21,"deviceId": 204,"ammonia":5,"carbonMonoxide": 40,"carbonDioxide": 280,"oxygen":16.9, "temprature":22.7,"humidity":64.9}
    pushToCloud(data); 


def sendData81():	
    data = {"houseId":1,"shadeId":21,"deviceId": 204,"ammonia":4.2,"carbonMonoxide":37,"carbonDioxide": 388,"oxygen":19.9, "temprature":28.5,"humidity":55.2}
    pushToCloud(data); 

def sendData82():	
    data = {"houseId":1,"shadeId":21,"deviceId": 204,"ammonia":5.1,"carbonMonoxide":50,"carbonDioxide": 299,"oxygen":16.9, "temprature":26.7,"humidity":44.4}
    pushToCloud(data); 

def sendData83():	
    data = {"houseId":1,"shadeId":21,"deviceId": 204,"ammonia":4.2,"carbonMonoxide":49,"carbonDioxide": 244,"oxygen":18.9, "temprature":27.7,"humidity":53.3}
    pushToCloud(data); 


def sendData84():	
    data = {"houseId":1,"shadeId":21,"deviceId": 204,"ammonia":4.7,"carbonMonoxide":38,"carbonDioxide": 277,"oxygen":18.5, "temprature":25.5,"humidity":64.9}
    pushToCloud(data); 


def sendData9():	
    data = {"houseId":1,"shadeId":31,"deviceId": 301,"ph":6.7,"tds":300,"turbidity": 45,"watemp":26.5}
    pushToCloud(data); 

def sendData91():	
    data = {"houseId":1,"shadeId":31,"deviceId": 301,"ph":7.1,"tds":299,"turbidity": 38,"watemp":22.5}
    pushToCloud(data); 

def sendData92():	
    data = {"houseId":1,"shadeId":31,"deviceId": 301,"ph":6.9,"tds":280,"turbidity": 50,"watemp":19.9}
    pushToCloud(data); 

def sendData93():	
    data = {"houseId":1,"shadeId":31,"deviceId": 301,"ph":6.7,"tds":304,"turbidity": 39,"watemp":19.4}
    pushToCloud(data); 

def sendData94():	
    data = {"houseId":1,"shadeId":31,"deviceId": 301,"ph":6.5,"tds":299,"turbidity": 44,"watemp":20.5}
    pushToCloud(data); 

def sendData10():   
    data = {"houseId":2,"shadeId":31,"deviceId": 302,"ammonia":5.1,"carbonMonoxide":44,"carbonDioxide": 300,"oxygen":18.5, "temprature":28.4,"humidity":56.0}
    pushToCloud(data); 


def sendData101():   
    data = {"houseId":2,"shadeId":31,"deviceId": 302,"ammonia":4.9,"carbonMonoxide":49,"carbonDioxide": 299,"oxygen":19.2, "temprature":28.4,"humidity":56.0}
    pushToCloud(data); 

def sendData102():   
    data = {"houseId":2,"shadeId":31,"deviceId": 302,"ammonia":3.7,"carbonMonoxide":45,"carbonDioxide": 280,"oxygen":18.9, "temprature":28.4,"humidity":56.0}
    pushToCloud(data); 


def sendData103():   
    data = {"houseId":2,"shadeId":31,"deviceId": 302,"ammonia":4.1,"carbonMonoxide":36,"carbonDioxide": 344,"oxygen":16.9, "temprature":28.4,"humidity":56.0}
    pushToCloud(data); 

def sendData104():   
    data = {"houseId":2,"shadeId":31,"deviceId": 302,"ammonia":3.9,"carbonMonoxide":44,"carbonDioxide": 277,"oxygen":18.2, "temprature":28.4,"humidity":56.0}
    pushToCloud(data); 

        
def sendData11():	
    data = {"houseId":1,"shadeId":31,"deviceId": 303,"ph":6.8,"tds":295,"turbidity": 48,"watemp":20.5}
    pushToCloud(data); 

def sendData111():	
    data = {"houseId":1,"shadeId":31,"deviceId": 303,"ph":6.9,"tds":300,"turbidity": 40,"watemp":21.5}
    pushToCloud(data); 

def sendData112():	
    data = {"houseId":1,"shadeId":31,"deviceId": 303,"ph":7.0,"tds":288,"turbidity": 39,"watemp":20.2}
    pushToCloud(data); 

def sendData113():	
    data = {"houseId":1,"shadeId":31,"deviceId": 303,"ph":6.8,"tds":266,"turbidity": 37,"watemp":21.7}
    pushToCloud(data); 
    
def sendData114():	
    data = {"houseId":1,"shadeId":31,"deviceId": 303,"ph":6.8,"tds":303,"turbidity": 48,"watemp":19.5}
    pushToCloud(data); 

def sendData12():   
    data = {"houseId":2,"shadeId":31,"deviceId": 304,"ammonia":4.5,"carbonMonoxide":48,"carbonDioxide": 280,"oxygen":19.1, "temprature":22.6,"humidity":53.0}
    pushToCloud(data); 

def sendData121():   
    data = {"houseId":2,"shadeId":31,"deviceId": 304,"ammonia":4.9,"carbonMonoxide":47,"carbonDioxide": 299,"oxygen":18.5, "temprature":21.0,"humidity":53.8}
    pushToCloud(data); 

def sendData122():   
    data = {"houseId":2,"shadeId":31,"deviceId": 304,"ammonia":5.0,"carbonMonoxide":37,"carbonDioxide": 305,"oxygen":17.9, "temprature":20.4,"humidity":53.2}
    pushToCloud(data); 

def sendData123():   
    data = {"houseId":2,"shadeId":31,"deviceId": 304,"ammonia":3.4,"carbonMonoxide":44,"carbonDioxide": 312,"oxygen":19.9, "temprature":28.6,"humidity":50.1}
    pushToCloud(data); 

def sendData124():   
    data = {"houseId":2,"shadeId":31,"deviceId": 304,"ammonia":3.9,"carbonMonoxide":40,"carbonDioxide": 270,"oxygen":19.5, "temprature":25.6,"humidity":59.0}
    pushToCloud(data); 

def sendData13():	
    data = {"houseId":1,"shadeId":41,"deviceId": 401,"ph":6.6,"tds":304,"turbidity": 49,"watemp":19.5}
    pushToCloud(data); 

def sendData131():	
    data = {"houseId":1,"shadeId":41,"deviceId": 401,"ph":6.7,"tds":299,"turbidity": 50,"watemp":17.4}
    pushToCloud(data); 

def sendData132():	
    data = {"houseId":1,"shadeId":41,"deviceId": 401,"ph":6.9,"tds":301,"turbidity": 39,"watemp":17.2}
    pushToCloud(data); 

def sendData133():	
    data = {"houseId":1,"shadeId":41,"deviceId": 401,"ph":7.0,"tds":299,"turbidity": 38,"watemp":18.9}
    pushToCloud(data); 

def sendData134():	
    data = {"houseId":1,"shadeId":41,"deviceId": 401,"ph":6.8,"tds":304,"turbidity": 47,"watemp":19.9}
    pushToCloud(data); 

def sendData14():   
    data = {"houseId":2,"shadeId":41,"deviceId": 402,"ammonia":5.2,"carbonMonoxide":49,"carbonDioxide": 330,"oxygen":19.2, "temprature":24.2,"humidity":58.5}
    pushToCloud(data); 

def sendData141():   
    data = {"houseId":2,"shadeId":41,"deviceId": 402,"ammonia":4.9,"carbonMonoxide":39,"carbonDioxide": 300,"oxygen":16.7, "temprature":27.2,"humidity":56.5}
    pushToCloud(data); 

def sendData142():   
    data = {"houseId":2,"shadeId":41,"deviceId": 402,"ammonia":4.9,"carbonMonoxide":45,"carbonDioxide": 290,"oxygen":18.9, "temprature":28.5,"humidity":54.8}
    pushToCloud(data); 

def sendData143():   
    data = {"houseId":2,"shadeId":41,"deviceId": 402,"ammonia":5,"carbonMonoxide":48,"carbonDioxide": 299,"oxygen":18.8, "temprature":29.9,"humidity":54.7}
    pushToCloud(data); 

def sendData144():   
    data = {"houseId":2,"shadeId":41,"deviceId": 402,"ammonia":3.2,"carbonMonoxide":42,"carbonDioxide": 288,"oxygen":18, "temprature":22.5,"humidity":59.5}
    pushToCloud(data); 

def sendData15():	
    data = {"houseId":1,"shadeId":41,"deviceId": 403,"ph":6.8,"tds":310,"turbidity": 44,"watemp":20.5}
    pushToCloud(data); 

def sendData151():	
    data = {"houseId":1,"shadeId":41,"deviceId": 403,"ph":6.5,"tds":280,"turbidity": 39,"watemp":18.9}
    pushToCloud(data); 

def sendData152():	
    data = {"houseId":1,"shadeId":41,"deviceId": 403,"ph":7.2,"tds":310,"turbidity": 42,"watemp":19.7}
    pushToCloud(data); 

def sendData153():	
    data = {"houseId":1,"shadeId":41,"deviceId": 403,"ph":7.3,"tds":309,"turbidity": 45,"watemp":19.5}
    pushToCloud(data); 

def sendData154():	
    data = {"houseId":1,"shadeId":41,"deviceId": 403,"ph":6.3,"tds":299,"turbidity": 50,"watemp":20.3}
    pushToCloud(data); 

def sendData16():   
    data = {"houseId":2,"shadeId":41,"deviceId": 404,"ammonia":4.8,"carbonMonoxide":48,"carbonDioxide": 41,"oxygen":17.5, "temprature":30.2,"humidity":55.2}
    pushToCloud(data); 

def sendData161():   
    data = {"houseId":2,"shadeId":41,"deviceId": 404,"ammonia":4.3,"carbonMonoxide":39,"carbonDioxide": 300,"oxygen":18.7, "temprature":27.2,"humidity":66.5}
    pushToCloud(data); 

def sendData162():   
    data = {"houseId":2,"shadeId":41,"deviceId": 404,"ammonia":4.8,"carbonMonoxide":48,"carbonDioxide": 290,"oxygen":19.9, "temprature":25.5,"humidity":64.8}
    pushToCloud(data); 

def sendData163():   
    data = {"houseId":2,"shadeId":41,"deviceId": 404,"ammonia":5,"carbonMonoxide":46,"carbonDioxide": 299,"oxygen":17.8, "temprature":24.9,"humidity":59.7}
    pushToCloud(data); 

def sendData164():   
    data = {"houseId":2,"shadeId":41,"deviceId": 404,"ammonia":3.9,"carbonMonoxide":41,"carbonDioxide": 288,"oxygen":18.2, "temprature":27.5,"humidity":51.5}
    pushToCloud(data); 

def sendData17():	
    data = {"houseId":1,"shadeId":51,"deviceId": 501,"ph":6.3,"tds":302,"turbidity": 47,"watemp":19.9}
    pushToCloud(data); 

def sendData171():	
    data = {"houseId":1,"shadeId":51,"deviceId": 501,"ph":6.7,"tds":299,"turbidity": 39,"watemp":18.9}
    pushToCloud(data); 

def sendData172():	
    data = {"houseId":1,"shadeId":51,"deviceId": 501,"ph":6.9,"tds":280,"turbidity": 44,"watemp":19.5}
    pushToCloud(data); 

def sendData173():	
    data = {"houseId":1,"shadeId":51,"deviceId": 501,"ph":7.1,"tds":299,"turbidity": 47,"watemp":18.9}
    pushToCloud(data); 

def sendData174():	
    data = {"houseId":1,"shadeId":51,"deviceId": 501,"ph":7.5,"tds":290,"turbidity": 39,"watemp":21.5}
    pushToCloud(data); 
        
def sendData18():   
    data = {"houseId":3,"shadeId":51,"deviceId": 502,"ammonia":5.4,"carbonMonoxide":42,"carbonDioxide": 297,"oxygen":16.9, "temprature":24.7,"humidity":53.0}
    pushToCloud(data); 

def sendData181():   
    data = {"houseId":2,"shadeId":51,"deviceId": 404,"ammonia":4.7,"carbonMonoxide":37,"carbonDioxide": 301,"oxygen":19.7, "temprature":26.2,"humidity":66.5}
    pushToCloud(data); 

def sendData182():   
    data = {"houseId":2,"shadeId":51,"deviceId": 404,"ammonia":3.8,"carbonMonoxide":46,"carbonDioxide": 287,"oxygen":19.1, "temprature":28.5,"humidity":64.8}
    pushToCloud(data); 

def sendData183():   
    data = {"houseId":2,"shadeId":51,"deviceId": 404,"ammonia":5.1,"carbonMonoxide":41,"carbonDioxide": 290,"oxygen":19.2, "temprature":25.9,"humidity":59.7}
    pushToCloud(data); 

def sendData184():   
    data = {"houseId":2,"shadeId":51,"deviceId": 404,"ammonia":3.9,"carbonMonoxide":48,"carbonDioxide": 308,"oxygen":18.2, "temprature":27.5,"humidity":51.5}
    pushToCloud(data); 

def sendData19():	
    data = {"houseId":1,"shadeId":51,"deviceId": 503,"ph":6.9,"tds":355,"turbidity": 50,"watemp":19.1}
    pushToCloud(data); 

def sendData191():	
    data = {"houseId":1,"shadeId":51,"deviceId": 503,"ph":6.7,"tds":299,"turbidity": 40,"watemp":19.4}
    pushToCloud(data); 

def sendData192():	
    data = {"houseId":1,"shadeId":51,"deviceId": 503,"ph":6.5,"tds":319,"turbidity": 39,"watemp":17.4}
    pushToCloud(data); 

def sendData193():	
    data = {"houseId":1,"shadeId":51,"deviceId": 503,"ph":7.1,"tds":309,"turbidity": 45,"watemp":18.8}
    pushToCloud(data); 

def sendData194():	
    data = {"houseId":1,"shadeId":51,"deviceId": 503,"ph":7.3,"tds":300,"turbidity": 48,"watemp":20.3}
    pushToCloud(data); 

def sendData20():   
    data = {"houseId":3,"shadeId":51,"deviceId": 504,"ammonia":5,"carbonMonoxide":47,"carbonDioxide": 288,"oxygen":19.5, "temprature":29.3,"humidity":55.0}
    pushToCloud(data); 

def sendData201():   
    data = {"houseId":3,"shadeId":51,"deviceId": 504,"ammonia":5.2,"carbonMonoxide":49,"carbonDioxide": 286,"oxygen":19.6, "temprature":27.4,"humidity":55.9}
    pushToCloud(data); 

def sendData202():   
    data = {"houseId":3,"shadeId":51,"deviceId": 504,"ammonia":4.3,"carbonMonoxide":50,"carbonDioxide": 244,"oxygen":18.5, "temprature":28.9,"humidity":57.5}
    pushToCloud(data); 

def sendData203():   
    data = {"houseId":3,"shadeId":51,"deviceId": 504,"ammonia":4.7,"carbonMonoxide":44,"carbonDioxide": 299,"oxygen":17.7, "temprature":26.2,"humidity":58.0}
    pushToCloud(data); 

def sendData204():   
    data = {"houseId":3,"shadeId":51,"deviceId": 504,"ammonia":4.8,"carbonMonoxide":43,"carbonDioxide": 288,"oxygen":18.4, "temprature":29.2,"humidity":55.1}
    pushToCloud(data); 

def sendData21():	
    data = {"houseId":1,"shadeId":61,"deviceId": 601,"ph":7.1,"tds":299,"turbidity": 35,"watemp":19.8}
    pushToCloud(data); 

def sendData211():	
    data = {"houseId":1,"shadeId":61,"deviceId": 601,"ph":6.9,"tds":300,"turbidity": 38,"watemp":19.6}
    pushToCloud(data); 

def sendData212():	
    data = {"houseId":1,"shadeId":61,"deviceId": 601,"ph":6.8,"tds":309,"turbidity": 39,"watemp":18.4}
    pushToCloud(data); 

def sendData213():	
    data = {"houseId":1,"shadeId":61,"deviceId": 601,"ph":7.0,"tds":299,"turbidity": 47,"watemp":20.8}
    pushToCloud(data); 

def sendData214():	
    data = {"houseId":1,"shadeId":61,"deviceId": 601,"ph":7.2,"tds":280,"turbidity": 50,"watemp":21.3}
    pushToCloud(data); 

def sendData22():   
    data = {"houseId":4,"shadeId":61,"deviceId": 602,"ammonia":4.9,"carbonMonoxide":42,"carbonDioxide": 289,"oxygen":17.9, "temprature":25.2,"humidity":54.2}
    pushToCloud(data); 

def sendData221():   
    data = {"houseId":4,"shadeId":61,"deviceId": 602,"ammonia":5.0,"carbonMonoxide":38,"carbonDioxide": 300,"oxygen":19.2, "temprature":26.7,"humidity":54.7}
    pushToCloud(data); 

def sendData222():   
    data = {"houseId":4,"shadeId":61,"deviceId": 602,"ammonia":4.7,"carbonMonoxide":45,"carbonDioxide": 299,"oxygen":14.8, "temprature":24.7,"humidity":55.6}
    pushToCloud(data); 
        
def sendData223():   
    data = {"houseId":4,"shadeId":61,"deviceId": 602,"ammonia":4.2,"carbonMonoxide":48,"carbonDioxide": 287,"oxygen":18.4, "temprature":27.6,"humidity":55.8}
    pushToCloud(data); 
        
def sendData224():   
    data = {"houseId":4,"shadeId":61,"deviceId": 602,"ammonia":3.9,"carbonMonoxide":40,"carbonDioxide": 303,"oxygen":18.9, "temprature":25.8,"humidity":57.6}
    pushToCloud(data); 

def sendData23():	
    data = {"houseId":1,"shadeId":61,"deviceId": 603,"ph":6.9,"tds":288,"turbidity": 47,"watemp":19.9}
    pushToCloud(data); 

    
def sendData231():	
    data = {"houseId":1,"shadeId":61,"deviceId": 603,"ph":7.2,"tds":294,"turbidity": 47,"watemp":20.1}
    pushToCloud(data); 

def sendData232():	
    data = {"houseId":1,"shadeId":61,"deviceId": 603,"ph":7.0,"tds":300,"turbidity": 47,"watemp":18.1}
    pushToCloud(data); 

def sendData233():	
    data = {"houseId":1,"shadeId":61,"deviceId": 603,"ph":6.7,"tds":299,"turbidity": 47,"watemp":19.6}
    pushToCloud(data); 

def sendData234():	
    data = {"houseId":1,"shadeId":61,"deviceId": 603,"ph":6.5,"tds":296,"turbidity": 47,"watemp":20.9}
    pushToCloud(data); 

def sendData24():   
    data = {"houseId":4,"shadeId":61,"deviceId": 604,"ammonia":5.0,"carbonMonoxide":48,"carbonDioxide": 299,"oxygen":17.7, "temprature":28.2,"humidity":58.8}
    pushToCloud(data); 

def sendData241():   
    data = {"houseId":4,"shadeId":61,"deviceId": 604,"ammonia":4.2,"carbonMonoxide":45,"carbonDioxide": 289,"oxygen":16.7, "temprature":27.2,"humidity":48.2}
    pushToCloud(data); 

def sendData242():   
    data = {"houseId":4,"shadeId":61,"deviceId": 604,"ammonia":3.9,"carbonMonoxide":48,"carbonDioxide": 279,"oxygen":18.5, "temprature":28.5,"humidity":51.5}
    pushToCloud(data); 

def sendData243():   
    data = {"houseId":4,"shadeId":61,"deviceId": 604,"ammonia":4.2,"carbonMonoxide":55,"carbonDioxide": 277,"oxygen":18.7, "temprature":30.2,"humidity":50.8}
    pushToCloud(data); 

def sendData244():   
    data = {"houseId":4,"shadeId":61,"deviceId": 604,"ammonia":5.1,"carbonMonoxide":40,"carbonDioxide": 284,"oxygen":17.7, "temprature":28.9,"humidity":58.1}
    pushToCloud(data); 

def pushToCloud(data):
    http = urllib3.PoolManager()
    response = http.request(METHOD,URL,body=json.dumps(data).encode('utf-8'),headers={'Content-Type': CONTENTTYPE})
    if response.status == 201:
        print("Data successfully pushed to cloud.")
    else:
        print("Failed to push the data to cloud.")



 #define timming  function
def main(argv):
    print("====================== Starting SMS PEMS Process ======================");
    schedule.every(EVERY_SECONDS1).seconds.do(sendData1);
    schedule.every(EVERY_SECONDS11).seconds.do(sendData11);
    schedule.every(EVERY_SECONDS12).seconds.do(sendData12);
    schedule.every(EVERY_SECONDS13).seconds.do(sendData13);
    schedule.every(EVERY_SECONDS14).seconds.do(sendData14);

    schedule.every(EVERY_SECONDS2).seconds.do(sendData2);
    schedule.every(EVERY_SECONDS21).seconds.do(sendData21);
    schedule.every(EVERY_SECONDS22).seconds.do(sendData22);
    schedule.every(EVERY_SECONDS23).seconds.do(sendData23);
    schedule.every(EVERY_SECONDS24).seconds.do(sendData24);


    schedule.every(EVERY_SECONDS3).seconds.do(sendData3);
    schedule.every(EVERY_SECONDS31).seconds.do(sendData31);
    schedule.every(EVERY_SECONDS32).seconds.do(sendData32);
    schedule.every(EVERY_SECONDS33).seconds.do(sendData33);
    schedule.every(EVERY_SECONDS34).seconds.do(sendData34);


    schedule.every(EVERY_SECONDS4).seconds.do(sendData4);
    schedule.every(EVERY_SECONDS41).seconds.do(sendData41);
    schedule.every(EVERY_SECONDS42).seconds.do(sendData42);
    schedule.every(EVERY_SECONDS43).seconds.do(sendData43);
    schedule.every(EVERY_SECONDS44).seconds.do(sendData44);


    schedule.every(EVERY_SECONDS5).seconds.do(sendData5);
    schedule.every(EVERY_SECONDS51).seconds.do(sendData51);
    schedule.every(EVERY_SECONDS52).seconds.do(sendData52);
    schedule.every(EVERY_SECONDS53).seconds.do(sendData53);
    schedule.every(EVERY_SECONDS54).seconds.do(sendData54);


    schedule.every(EVERY_SECONDS6).seconds.do(sendData6);
    schedule.every(EVERY_SECONDS61).seconds.do(sendData61);
    schedule.every(EVERY_SECONDS62).seconds.do(sendData62);
    schedule.every(EVERY_SECONDS63).seconds.do(sendData63);
    schedule.every(EVERY_SECONDS64).seconds.do(sendData64);


    schedule.every(EVERY_SECONDS7).seconds.do(sendData7);
    schedule.every(EVERY_SECONDS71).seconds.do(sendData71);
    schedule.every(EVERY_SECONDS72).seconds.do(sendData72);
    schedule.every(EVERY_SECONDS73).seconds.do(sendData73);
    schedule.every(EVERY_SECONDS74).seconds.do(sendData74);


    schedule.every(EVERY_SECONDS8).seconds.do(sendData8);
    schedule.every(EVERY_SECONDS81).seconds.do(sendData81);
    schedule.every(EVERY_SECONDS82).seconds.do(sendData82);
    schedule.every(EVERY_SECONDS83).seconds.do(sendData83);
    schedule.every(EVERY_SECONDS84).seconds.do(sendData84);
    

    schedule.every(EVERY_SECONDS9).seconds.do(sendData9);
    schedule.every(EVERY_SECONDS91).seconds.do(sendData91);
    schedule.every(EVERY_SECONDS92).seconds.do(sendData92);
    schedule.every(EVERY_SECONDS93).seconds.do(sendData93);
    schedule.every(EVERY_SECONDS94).seconds.do(sendData94);

    
    schedule.every(EVERY_SECONDS10).seconds.do(sendData10);
    schedule.every(EVERY_SECONDS101).seconds.do(sendData101);
    schedule.every(EVERY_SECONDS102).seconds.do(sendData102);
    schedule.every(EVERY_SECONDS103).seconds.do(sendData103);
    schedule.every(EVERY_SECONDS104).seconds.do(sendData104);

    schedule.every(EVERY_SECONDS11).seconds.do(sendData11);
    schedule.every(EVERY_SECONDS111).seconds.do(sendData111);
    schedule.every(EVERY_SECONDS112).seconds.do(sendData112);
    schedule.every(EVERY_SECONDS113).seconds.do(sendData113);
    schedule.every(EVERY_SECONDS114).seconds.do(sendData114);

    schedule.every(EVERY_SECONDS12).seconds.do(sendData12);
    schedule.every(EVERY_SECONDS121).seconds.do(sendData121);
    schedule.every(EVERY_SECONDS122).seconds.do(sendData122);
    schedule.every(EVERY_SECONDS123).seconds.do(sendData123);
    schedule.every(EVERY_SECONDS124).seconds.do(sendData124);

    schedule.every(EVERY_SECONDS13).seconds.do(sendData13);
    schedule.every(EVERY_SECONDS131).seconds.do(sendData131);
    schedule.every(EVERY_SECONDS132).seconds.do(sendData132);
    schedule.every(EVERY_SECONDS133).seconds.do(sendData133);
    schedule.every(EVERY_SECONDS134).seconds.do(sendData134);

    schedule.every(EVERY_SECONDS14).seconds.do(sendData14);
    schedule.every(EVERY_SECONDS141).seconds.do(sendData141);
    schedule.every(EVERY_SECONDS142).seconds.do(sendData142);
    schedule.every(EVERY_SECONDS143).seconds.do(sendData143);
    schedule.every(EVERY_SECONDS144).seconds.do(sendData144);

    schedule.every(EVERY_SECONDS15).seconds.do(sendData15);
    schedule.every(EVERY_SECONDS151).seconds.do(sendData151);
    schedule.every(EVERY_SECONDS152).seconds.do(sendData152);
    schedule.every(EVERY_SECONDS153).seconds.do(sendData153);
    schedule.every(EVERY_SECONDS154).seconds.do(sendData154);

    schedule.every(EVERY_SECONDS16).seconds.do(sendData16);
    schedule.every(EVERY_SECONDS161).seconds.do(sendData161);
    schedule.every(EVERY_SECONDS162).seconds.do(sendData162);
    schedule.every(EVERY_SECONDS163).seconds.do(sendData163);
    schedule.every(EVERY_SECONDS164).seconds.do(sendData164);

    schedule.every(EVERY_SECONDS17).seconds.do(sendData17);
    schedule.every(EVERY_SECONDS17).seconds.do(sendData171);
    schedule.every(EVERY_SECONDS17).seconds.do(sendData172);
    schedule.every(EVERY_SECONDS17).seconds.do(sendData173);
    schedule.every(EVERY_SECONDS17).seconds.do(sendData174);

    schedule.every(EVERY_SECONDS18).seconds.do(sendData18);
    schedule.every(EVERY_SECONDS181).seconds.do(sendData181);
    schedule.every(EVERY_SECONDS182).seconds.do(sendData182);
    schedule.every(EVERY_SECONDS183).seconds.do(sendData183);
    schedule.every(EVERY_SECONDS184).seconds.do(sendData184);

    schedule.every(EVERY_SECONDS19).seconds.do(sendData19);
    schedule.every(EVERY_SECONDS191).seconds.do(sendData191);
    schedule.every(EVERY_SECONDS192).seconds.do(sendData192);
    schedule.every(EVERY_SECONDS193).seconds.do(sendData193);
    schedule.every(EVERY_SECONDS194).seconds.do(sendData194);

    
    schedule.every(EVERY_SECONDS20).seconds.do(sendData20);
    schedule.every(EVERY_SECONDS201).seconds.do(sendData201);
    schedule.every(EVERY_SECONDS202).seconds.do(sendData202);
    schedule.every(EVERY_SECONDS203).seconds.do(sendData203);
    schedule.every(EVERY_SECONDS204).seconds.do(sendData204);

    schedule.every(EVERY_SECONDS21).seconds.do(sendData21);
    schedule.every(EVERY_SECONDS211).seconds.do(sendData211);
    schedule.every(EVERY_SECONDS212).seconds.do(sendData212);
    schedule.every(EVERY_SECONDS213).seconds.do(sendData213);
    schedule.every(EVERY_SECONDS214).seconds.do(sendData214);

    schedule.every(EVERY_SECONDS22).seconds.do(sendData22);
    schedule.every(EVERY_SECONDS221).seconds.do(sendData221);
    schedule.every(EVERY_SECONDS222).seconds.do(sendData222);
    schedule.every(EVERY_SECONDS223).seconds.do(sendData223);
    schedule.every(EVERY_SECONDS224).seconds.do(sendData224);


    schedule.every(EVERY_SECONDS23).seconds.do(sendData23);
    schedule.every(EVERY_SECONDS231).seconds.do(sendData231);
    schedule.every(EVERY_SECONDS232).seconds.do(sendData232);
    schedule.every(EVERY_SECONDS233).seconds.do(sendData233);
    schedule.every(EVERY_SECONDS234).seconds.do(sendData234);


    schedule.every(EVERY_SECONDS24).seconds.do(sendData24);
    schedule.every(EVERY_SECONDS241).seconds.do(sendData241);
    schedule.every(EVERY_SECONDS242).seconds.do(sendData242);
    schedule.every(EVERY_SECONDS243).seconds.do(sendData243);
    schedule.every(EVERY_SECONDS244).seconds.do(sendData244);

##    #schedule.every(EVERY_SECONDS).seconds.do(sendData25);
##

    


    while True: 
         schedule.run_pending()
         #time.sleep()
         #print("====================== Shutting down SMS PEMS Process ======================");
    
    
if __name__ == "__main__":
    main(sys.argv)
