import serial
import serial
import json
import urllib2
import datetime
import serial
import os, time
import schedule
import sys

arduino = serial.Serial("/dev/ttyACM0",9600)#data read from arduino port

#arduino = serial.Serial("/dev/ttyACM1",9600)

# Enable Serial Communication
#port = serial.Serial("/dev/ttyS0", baudrate=9600, timeout=1)



EVERY_SECONDS = 120;  # SECONDS * WITH  MINTS
Minit = 1800 ;
hour = 1800;

def sendData():
    try:
      values   = arduino.readline()
      nh3 =  values.split("|")[0]
 #     oxygen    = values.split("|")[1]
      lpg    = values.split("|")[1]
      co     = values.split("|")[2]
      smoke  = values.split("|")[3]
      pm2 =    values.split("|")[4]
      flame =  values.split("|")[5]
      co2    = values.split("|")[6]
      oxygen    = values.split("|")[7]
      temp   = values.split("|")[8]
      humidity    = values.split("|")[9]

      data = {"deviceId": 3,"nh3":nh3,"lpg":lpg,"co":co,"smoke":smoke,"pm2":pm2,"flame":flame,"co2": co2,"oxygen":oxygen, "temp":temp,"humidity":humidity,"captured": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}
      request = urllib2.Request("http://apps.dignous.com/dignousraw/epadevices/add")
      request.add_header('Content-Type', 'application/json')
      response = urllib2.urlopen(request, json.dumps(data));
      if response.getcode() == 201:
           print("Data successfully pushed to cloud.")
#       else:
#          print("Failed to push the data to cloud.")
    except:
         print("error")


# Transmitting AT Commands to the Modem
# '\r\n' indicates the Enter key
port.write('AT'+'\r\n')
rcv = port.read(10)
print rcv

# set number to send person in class
def startSMSProcess():
    try:
       values   = arduino.readline()
       nh3 =  values.split("|")[0]
       lpg    = values.split("|")[1]
       co     = values.split("|")[2]
       smoke  = values.split("|")[3]
       pm2 =    values.split("|")[4]
       flame =  values.split("|")[5]
       co2    = values.split("|")[6]
       oxygen    = values.split("|")[7]
       temp   = values.split("|")[8]
       humidity    = values.split("|")[9]
      


       port.write("AT+CMGF=1\r\n")
       rcv = port.read(10)
       print rcv
       time.sleep(2)

       port.write('AT+CMGS="+923334359701"'+'\r\n')
    

       rcv = port.read(10)
       print rcv
       time.sleep(2)

       port.write('Air Lahore Site Summary\r\n'+'\r\n')


       port.write("Temp in C:" + (temp +'\r\n')) 
#       port.write("RH in %:" + (humidity +'\r\n')) 
       port.write("CO2 in ppm:" + (co2 +'\r\n')) 
       port.write("NH3 in ppm:" + (nh3 +'\r\n')) 
       port.write("PM2.5 in ug/m3:" + (pm2 +'\r\n')) 
       port.write("Oxygen in % =" + (oxygen +'\r\n')) 
       port.write("CO in ppm:" + (co +'\r\n')) 
       if smoke == 0:
           port.write("SMOKE:smoke" +'\r\n') 
       else:
           port.write("SMOKE:No smoke" +'\r\n') 
       if flame == 0:
           port.write("fire detected:" +'\r\n') 
       else:
           port.write("No fire detected:" +'\r\n')
       if lpg == 0:
           port.write("LPG:detect"+'\r\n')

       else:
           port.write("LPG:not detect"+'\r\n')
       port.write("RH in %:" + (humidity +'\r\n')) 



       rcv = port.read(1)
       print(rcv)
       port.write("\x1A")
       for i in range(10):
           rcv = port.read(10)
           print rcv
    except:
           print("error")
 
startSMSProcess() # End function

# set number to send person in class
def startSMSProcess2():
    try:
       values   = arduino.readline()
       nh3 =  values.split("|")[0]
       lpg    = values.split("|")[1]
       co     = values.split("|")[2]
       smoke  = values.split("|")[3]
       pm2 =    values.split("|")[4]
       flame =  values.split("|")[5]
       co2    = values.split("|")[6]
       oxygen    = values.split("|")[7]
       temp   = values.split("|")[8]
       humidity    = values.split("|")[9]
      


       port.write("AT+CMGF=1\r\n")
       rcv = port.read(10)
       print rcv
       time.sleep(2)

       port.write('AT+CMGS="+923162645177"'+'\r\n')
    

       rcv = port.read(10)
       print rcv
       time.sleep(2)

       port.write('Air Lahore Site Summary\r\n'+'\r\n')


       port.write("Temp in C:" + (temp +'\r\n')) 
#       port.write("RH in %:" + (humidity +'\r\n')) 
       port.write("CO2 in ppm:" + (co2 +'\r\n')) 
       port.write("NH3 in ppm:" + (nh3 +'\r\n')) 
       port.write("PM2.5 in ug/m3:" + (pm2 +'\r\n')) 
       port.write("Oxygen in % =" + (oxygen +'\r\n')) 
       port.write("CO in ppm:" + (co +'\r\n')) 
       if smoke == 0:
           port.write("SMOKE:smoke" +'\r\n') 
       else:
           port.write("SMOKE:No smoke" +'\r\n') 
       if flame == 0:
           port.write("fire detected:" +'\r\n') 
       else:
           port.write("No fire detected:" +'\r\n')
       if lpg == 0:
           port.write("LPG:detect"+'\r\n')

       else:
           port.write("LPG:not detect"+'\r\n')
       port.write("RH in %:" + (humidity +'\r\n')) 



       rcv = port.read(1)
       print(rcv)
       port.write("\x1A")
       for i in range(10):
           rcv = port.read(10)
           print rcv
    except:
           print("error")
 
startSMSProcess2() # End function



 #define timming  function
def main(argv):
    print("====================== Starting SMS PEMS Process ======================");
    schedule.every(EVERY_SECONDS).seconds.do(sendData);
    schedule.every(Minit).seconds.do(startSMSProcess);
    schedule.every(hour).seconds.do(startSMSProcess2);


    while True: 
         schedule.run_pending()
         time.sleep(1)
         print("====================== Shutting down SMS PEMS Process ======================");
    
    
if __name__ == "__main__":
    main(sys.argv)



